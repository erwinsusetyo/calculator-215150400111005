package com.example.kalkulator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button bt0, bt1, bt2, bt3, bt4, bt5, bt6, bt7, bt8, bt9,
            btTambah, btKurang, btKali, btBagi,btHasil, btClear;
    TextView tv1, tv2, tv3, tvHasil;
    int val1 = 0, val2 = 0;
    boolean isTambah, isKurang, isKali, isBagi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tv1 = (TextView) findViewById(R.id.tv1);
        tv2 = (TextView) findViewById(R.id.tv2);
        tv3 = (TextView) findViewById(R.id.tv3);
        tvHasil = (TextView) findViewById(R.id.tvHasil);
        bt1 = (Button) findViewById(R.id.bt1);
        bt1.setOnClickListener(ocl1);
        bt2 = (Button) findViewById(R.id.bt2);
        bt2.setOnClickListener(ocl2);
        bt3 = (Button) findViewById(R.id.bt3);
        bt3.setOnClickListener(ocl3);
        bt4 = (Button) findViewById(R.id.bt4);
        bt4.setOnClickListener(ocl4);
        bt5 = (Button) findViewById(R.id.bt5);
        bt5.setOnClickListener(ocl5);
        bt6 = (Button) findViewById(R.id.bt6);
        bt6.setOnClickListener(ocl6);
        bt7 = (Button) findViewById(R.id.bt7);
        bt7.setOnClickListener(ocl7);
        bt8 = (Button) findViewById(R.id.bt8);
        bt8.setOnClickListener(ocl8);
        bt9 = (Button) findViewById(R.id.bt9);
        bt9.setOnClickListener(ocl9);
        bt0 = (Button) findViewById(R.id.bt0);
        bt0.setOnClickListener(ocl0);
        btTambah = (Button) findViewById(R.id.btTambah);
        btTambah.setOnClickListener(oclTambah);
        btKurang = (Button) findViewById(R.id.btKurang);
        btKurang.setOnClickListener(oclKurang);
        btKali = (Button) findViewById(R.id.btKali);
        btKali.setOnClickListener(oclKali);
        btBagi = (Button) findViewById(R.id.btBagi);
        btBagi.setOnClickListener(oclBagi);
        btHasil = (Button) findViewById(R.id.btHasil);
        btHasil.setOnClickListener(oclHasil);
        btClear = (Button) findViewById(R.id.btClear);
        btClear.setOnClickListener(oclClear);
    }

    private View.OnClickListener ocl1 = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            tv2.setText(tv2.getText() + "1");
            tv3.setText(tv3.getText() + "1");
        }
    };
    private View.OnClickListener ocl2 = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            tv2.setText(tv2.getText() + "2");
            tv3.setText(tv3.getText() + "2");
        }
    };
    private View.OnClickListener ocl3 = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            tv2.setText(tv2.getText() + "3");
            tv3.setText(tv3.getText() + "3");
        }
    };
    private View.OnClickListener ocl4 = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            tv2.setText(tv2.getText() + "4");
            tv3.setText(tv3.getText() + "4");
        }
    };
    private View.OnClickListener ocl5 = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            tv2.setText(tv2.getText() + "5");
            tv3.setText(tv3.getText() + "5");
        }
    };
    private View.OnClickListener ocl6 = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            tv2.setText(tv2.getText() + "6");
            tv3.setText(tv3.getText() + "6");
        }
    };
    private View.OnClickListener ocl7 = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            tv2.setText(tv2.getText() + "7");
            tv3.setText(tv3.getText() + "7");
        }
    };
    private View.OnClickListener ocl8 = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            tv2.setText(tv2.getText() + "8");
            tv3.setText(tv3.getText() + "8");
        }
    };
    private View.OnClickListener ocl9 = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            tv2.setText(tv2.getText() + "9");
            tv3.setText(tv3.getText() + "9");
        }
    };
    private View.OnClickListener ocl0 = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            tv2.setText(tv2.getText() + "0");
            tv3.setText(tv3.getText() + "0");
        }
    };

    private View.OnClickListener oclTambah = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(tv2 == null) {
                tv2.setText("");
            } else {
                val1 = Integer.parseInt(tv2.getText() + "");
                isTambah = true;
                tv2.setText(null);
                tv3.setText(tv3.getText() + " + ");
            }
        }
    };
    private View.OnClickListener oclKurang = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(tv2 == null) {
                tv2.setText("");
            } else {
                val1 = Integer.parseInt(tv2.getText() + "");
                isKurang = true;
                tv2.setText(null);
                tv3.setText(tv3.getText() + " - ");
            }
        }
    };
    private View.OnClickListener oclKali = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(tv2 == null) {
                tv2.setText("");
            } else {
                val1 = Integer.parseInt(tv2.getText() + "");
                isKali = true;
                tv2.setText(null);
                tv3.setText(tv3.getText() + " * ");
            }
        }
    };
    private View.OnClickListener oclBagi = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(tv2 == null) {
                tv2.setText("");
            } else {
                val1 = Integer.parseInt(tv2.getText() + "");
                isBagi = true;
                tv2.setText(null);
                tv3.setText(tv3.getText() + " / ");
            }
        }
    };

    private View.OnClickListener oclHasil = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            val2 = Integer.parseInt(tv2.getText() + "");
            tv2.setText(null);
            if(isTambah) {
                tvHasil.setText(val1 + val2 + "");
                isTambah = false;
            }
            if(isKurang) {
                tvHasil.setText(val1 - val2 + "");
                isKurang = false;
            }
            if(isKali) {
                tvHasil.setText(val1 * val2 + "");
                isKali = false;
            }
            if(isBagi) {
                tvHasil.setText(val1 / val2 + "");
                isBagi = false;
            }
        }
    };
    private View.OnClickListener oclClear = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            tv2.setText(null);
            tv3.setText(null);
            tvHasil.setText(null);
        }
    };
}
